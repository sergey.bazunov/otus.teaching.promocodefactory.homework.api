﻿using System;

namespace Otus.Teaching.PromoCodeFactory.GraphQL.Api.Models
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}
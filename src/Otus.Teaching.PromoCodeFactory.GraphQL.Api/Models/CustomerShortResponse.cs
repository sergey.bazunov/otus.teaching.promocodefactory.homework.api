﻿using System;

namespace Otus.Teaching.PromoCodeFactory.GraphQL.Api.Models
{
    public class CustomerShortResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}

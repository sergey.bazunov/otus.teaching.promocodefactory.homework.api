﻿using HotChocolate;
using HotChocolate.Types;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GraphQL.Api.Mappers;
using Otus.Teaching.PromoCodeFactory.GraphQL.Api.Models;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GraphQL.Api.MutationResolver
{
    [ExtendObjectType("Mutation")]
    public class CustomerMutationResolver
    {
        /* mutation text for tests:

            mutation ($req:CreateOrEditCustomerRequestInput) {
                createCustomer(request: $req)  
            }

        where variable:

             "req": {
                "email": "serb@asthenis.de",
                "firstName": "Serb",
                "lastName": "Baz",
                "preferenceIds": ["ef7f299f-92d7-459f-896e-078ed53ef99c"]
              }
         */

        public async Task<Guid> CreateCustomer(CreateOrEditCustomerRequest request, 
            [Service] IRepository<Preference> _preferenceRepository,
            [Service] IRepository<Customer> _customerRepository)
        {            
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            return customer.Id;
        }


        // Good Exception Handling example with graphql: (just place it here)
        // https://medium.com/@TimHolzherr/creating-a-graphql-backend-in-c-how-to-get-started-with-hot-chocolate-12-in-net-6-30f0fb177c5c
        
        public async Task<Guid> EditCustomer(Guid id, CreateOrEditCustomerRequest request,
            [Service] IRepository<Preference> _preferenceRepository,
            [Service] IRepository<Customer> _customerRepository)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            
            if (customer == null)
                throw new ApplicationException("Customer not found");

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return customer.Id;
        }


        public async Task<Guid> DeleteCustomer(Guid id, [Service] IRepository<Customer> _customerRepository)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                throw new ApplicationException("Customer not found");

            await _customerRepository.DeleteAsync(customer);

            return customer.Id;
        }

    }
}

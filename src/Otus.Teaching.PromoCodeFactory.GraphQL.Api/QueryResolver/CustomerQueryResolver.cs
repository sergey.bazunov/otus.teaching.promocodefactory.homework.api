﻿using HotChocolate;
using HotChocolate.Types;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GraphQL.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GraphQL.Api.QueryResolver
{
    [ExtendObjectType("Query")]
    public class CustomerQueryResolver
    {
        // query {
        //  customer(id: "a6c8c6b1-4349-45b0-ab31-244740aaf0f0") {
        //    id,
        //    firstName,
        //    lastName,
        //    email,
        //    preferences {
        //      name
        //    }
        //  }
        // } 
        public async Task<CustomerResponse> GetCustomer(Guid id, [Service] IRepository<Customer> _customerRepository)
        {
            Customer customer = await _customerRepository.GetByIdAsync(id);
            var response = new CustomerResponse(customer);
            return response;
        }

         // query {
         //  customers {
         //   firstName,
         //   lastName,
         //   email
         // }
        public async Task<List<CustomerShortResponse>> GetCustomers([Service] IRepository<Customer> _customerRepository)
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return response;
        }
    }
}

﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GrpcService.Services
{
    public class CustomersService : Customers.CustomersBase
    {
        private readonly ILogger<CustomersService> logger;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersService(ILogger<CustomersService> logger, IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            this.logger = logger;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<CustomersShortResponse> GetCustomers(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();
            CustomersShortResponse response = new CustomersShortResponse();
            foreach (var customer in customers)
                response.Items.Add(new CustomerShortResponse()
                {
                    Id = customer.Id.ToString(),
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Email = customer.Email
                });
            return response;
        }

        public override async Task<CustomerResponse> GetCustomer(CustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            if (customer == null)
            {
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found"));
            }

            var response = new CustomerResponse()
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,                
            };

            if (customer.Preferences.Any())
            {
                response.Preferences.AddRange(
                    customer.Preferences.Select(x => new PreferenceResponse()
                    {
                        Id = x.PreferenceId.ToString(),
                        Name = x.Preference.Name
                    }));
            }                        

            return response;
            
        }

        public override async Task<CreateCustomerResponse> CreateCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {
            IEnumerable<Preference> preferences = new List<Preference>();
            if (request.PreferenceIds.Any())
            { 
                List<Guid> prefs = request.PreferenceIds.AsEnumerable().Select(p => new Guid(p)).ToList();
                preferences = await _preferenceRepository.GetRangeByIdsAsync(prefs);
            }
            
            Customer customer = CustomerMapper.MapFromModel(request, preferences);
            await _customerRepository.AddAsync(customer);

            return new CreateCustomerResponse() { Id = customer.Id.ToString() };
        }

        public override async Task<Empty> EditCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found."));

            IEnumerable<Preference> preferences = new List<Preference>();
            if (request.PreferenceIds.Any())
            {
                List<Guid> prefs = request.PreferenceIds.AsEnumerable().Select(p => new Guid(p)).ToList();
                preferences = await _preferenceRepository.GetRangeByIdsAsync(prefs);
            }

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return new Empty();
        }

        public override async Task<Empty> DeleteCustomer(CustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found."));

            await _customerRepository.DeleteAsync(customer);

            return new Empty();

        }
    }
}
